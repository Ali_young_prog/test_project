package com.example.test_project

data class User(
    val id: Int,
    val username: String,
    val firstName: String,
    val secondName: String,
    val email: String,
    val password: String,
    val phone: String,
    val userSatatus: Int
)
