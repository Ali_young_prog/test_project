package com.example.test_project

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Sorry()
        findViewById<Button>(R.id.btn_login).setOnClickListener{v : View ->
            val b = BottomSheetDialog()
            b.show(this.supportFragmentManager, "")
        }

        findViewById<Button>(R.id.btn_info).setOnClickListener{v : View ->
            val intent = Intent(this, About::class.java)
            startActivity(intent)
        }
    }
    fun Sorry (){
        Toast.makeText(applicationContext, "Очень мало времени :(, но всеравно спасибо за задание.", Toast.LENGTH_LONG).show()
    }
}